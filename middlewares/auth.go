package middlewares

import (
	"crypto/subtle"
	"os"

	"github.com/labstack/echo/v4"
)

func BasicAuth(username, password string, c echo.Context) (bool, error) {
	envUsername := os.Getenv("APP_USERNAME")
	envPassword := os.Getenv("APP_PASSWORD")

	if subtle.ConstantTimeCompare([]byte(username), []byte(envUsername)) == 1 &&
		subtle.ConstantTimeCompare([]byte(password), []byte(envPassword)) == 1 {
		return true, nil
	}
	return false, nil
}
