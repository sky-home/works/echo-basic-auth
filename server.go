package main

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"gitlab.com/meisskywalker/echo-basic-auth/routes"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Can not read .env")
	}

	e := echo.New()

	routes.RegisterRoutes(e)

	e.Logger.Fatal(e.Start(":1323"))
}
