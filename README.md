# Go Echo Basic Auth With Environment Variable

Cara menjalankan aplikasi:

```
$ go run server.go
```

Kredensial dalam `.env`:

```
username: bayu
passwoed: rahasia
```

Routes:
| Endpoint | Middleware |
| -------- | ---------- |
| `/`      | -          |
| `/admin` | Auth Basic |
