package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/meisskywalker/echo-basic-auth/middlewares"
)

func RegisterRoutes(server *echo.Echo) {
	server.GET("/", Home)

	admin := server.Group("/admin")

	admin.Use(middleware.BasicAuth(middlewares.BasicAuth))

	admin.GET("", Admin)
}

func Home(c echo.Context) error {
	return c.String(http.StatusOK, "Welcome to /")
}

func Admin(c echo.Context) error {
	return c.String(http.StatusOK, "Welcome to /admin")
}
